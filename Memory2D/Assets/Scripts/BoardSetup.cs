﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardSetup : MonoBehaviour {

    private int rows = PlayerData.numberOfRows_Columns;
    private int columns = PlayerData.numberOfRows_Columns;
    private Transform boardTransform;
    public GameObject cardObject;
    public GameObject testCardObject;
    public Sprite[] availableSprites;
    public List<Sprite> chosenSprites = new List<Sprite>();

    private void ChooseSpritesToUse()
    {
        Sprite spriteToUse;
        int i = 0;
        while(i < (rows * columns / 2))
        {
            spriteToUse = availableSprites[Random.Range(0, availableSprites.Length-1)];
            if (!chosenSprites.Contains(spriteToUse))
            {
                chosenSprites.Add(spriteToUse);
                chosenSprites.Add(spriteToUse);
                i++;
            }
        }
    }

    void BoardSetupFunction()
    {
        Sprite spriteToForward;
        boardTransform = new GameObject("Board").transform;
        ChooseSpritesToUse();

        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                GameObject instance = Instantiate(cardObject, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                spriteToForward = chosenSprites[Random.Range(0, chosenSprites.Count)];
                instance.GetComponent<CardScript>().pokemonImage = spriteToForward;
                chosenSprites.Remove(spriteToForward);
                //Debug.Log("Card is at position x= " + x + " y=" + y);

                instance.transform.SetParent(boardTransform);
            }
        }
    }

    // Use this for initialization
    public void SetupPlayingBoard () {

        //Instantiate(testCardObject, new Vector3(1,1,0f), Quaternion.identity);
        //Instantiate(testCardObject, new Vector3(0, 0, 0f), Quaternion.identity);
        BoardSetupFunction();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
