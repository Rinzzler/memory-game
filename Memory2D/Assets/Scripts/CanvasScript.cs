﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class CanvasScript : MonoBehaviour {

    public GameObject panel_1, panel_2, panel_3;
    public GameObject dropdown;
    
    // Use this for initialization
	void Start () {
        panel_2.SetActive(false);
        panel_3.SetActive(false);
	}

    public void ExitGame()
    {
        Application.Quit();
    }

    public void GoToSizeOfField()
    {
        panel_1.SetActive(false);
        panel_2.SetActive(true);
    }

    public void ChangeSizeOfField()
    {
        PlayerData.numberOfRows_Columns = (int)char.GetNumericValue(dropdown.GetComponentInChildren<TextMeshProUGUI>().text[0]);
        Debug.Log(PlayerData.numberOfRows_Columns);
    }


    public void StartGame()
    {
        
        SceneManager.LoadScene("GameScene");
    }

    public void ShowScores()
    {
        panel_1.SetActive(false);
        panel_3.SetActive(true);
        gameObject.GetComponent<GetRequest>().StartGetText();
    }

    public void BackToMainMenu()
    {
        panel_2.SetActive(false);
        panel_3.SetActive(false);
        panel_1.SetActive(true);
    }
}
