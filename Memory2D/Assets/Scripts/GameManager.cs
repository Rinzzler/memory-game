﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public List <GameObject> cardsToCompare;
    private BoardSetup boardScript;
    public bool canClick = true;
    public GameObject screenText;
    private int pairedUp = 0;
    public GameObject canvasGame;
    public float startingTime, endTime;

    void CompareCards()
    {
        canClick = false;
        if (cardsToCompare[0].GetComponent<SpriteRenderer>().sprite == cardsToCompare[1].GetComponent<SpriteRenderer>().sprite)
        {
            Debug.Log("Iste su");
            for (int i = 0; i < cardsToCompare.Count; i++)
            {
                cardsToCompare[i].GetComponent<CardScript>().RemoveFromField();
                pairedUp++;
            }
            if (pairedUp == PlayerData.numberOfRows_Columns * PlayerData.numberOfRows_Columns)
                GameOver();
        }
        else
            for (int i = 0; i < cardsToCompare.Count; i++)
                cardsToCompare[i].GetComponent<CardScript>().ReturnToOriginalPosition();
        cardsToCompare.Clear();
    }

    public void AddCardToCompare(GameObject card)
    {
        PlayerData.numberofClicks += 1;
        screenText.GetComponent<TextMeshProUGUI>().SetText("Clicks: " + PlayerData.numberofClicks);
        cardsToCompare.Add(card);
        if (cardsToCompare.Count == 2)
            CompareCards();
    }



    void Awake()
    {
        Debug.Log(PlayerData.numberOfRows_Columns);
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(gameObject);
        boardScript = GetComponent<BoardSetup>();
        canvasGame.GetComponent<EndGameScript>().panel_2.SetActive(false);
        canvasGame.GetComponent<EndGameScript>().panel_1.SetActive(false);
        startingTime = Time.time;
        InitGame();
    }

    void InitGame()
    {
        boardScript.SetupPlayingBoard();
    }

    public void GameOver()
    {
        endTime = Time.time - startingTime;
        gameObject.GetComponent<FormatTime>().Format(endTime);
        canvasGame.GetComponent<EndGameScript>().panel_2.SetActive(true);
        canvasGame.GetComponent<EndGameScript>().ShowScore();
        enabled = false;
    }
}
