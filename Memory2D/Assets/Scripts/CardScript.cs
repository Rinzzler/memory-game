﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardScript : MonoBehaviour
{

    private SpriteRenderer spriteRenderer;
    private Vector3 initialTransform;
    private Sprite initialSprite;
    public Sprite pokemonImage;
    public Sprite[] availableSprites1;
    private Coroutine changeScaleCoroutine;
    private bool cardTurned = false;
    private Vector3 targetTransform;



    private IEnumerator DelayAndDestroy(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
        GameManager.instance.canClick = true;
    }

    private IEnumerator ChangeScale(Vector3 targetSize, float time = 0f, bool flipCard = false, bool enableClick = false)
    {
        float fraction = 0;
        float timeToChange = 0.2f;
        Vector3 pocetak = transform.localScale;
        if (time > 0f)
            yield return new WaitForSeconds(time);
        while (fraction < 1)
        {
            fraction += (Time.deltaTime / timeToChange);
            transform.localScale = Vector3.Lerp(pocetak, targetSize, fraction);
            //spriteRenderer.size =;
            yield return null;
        }
        if (flipCard)
        {
            spriteRenderer.sprite = initialSprite;
            cardTurned = false;
        }
        if (enableClick)
        {
            //Debug.Log("CAN CLICK");
            GameManager.instance.canClick = true;
        }
    }

    private void OnMouseEnter()
    {
        //Debug.Log("Mouse is over object");
        if (cardTurned == false)
        {
            if (changeScaleCoroutine != null)
                StopCoroutine(changeScaleCoroutine);
            changeScaleCoroutine = StartCoroutine(ChangeScale(targetTransform));
        }
    }

    private void OnMouseExit()
    {
        //Debug.Log("Mouse is not over object");
        if (spriteRenderer.sprite != pokemonImage)
        {
            StopCoroutine(changeScaleCoroutine);
            changeScaleCoroutine = StartCoroutine(ChangeScale(initialTransform));
        }
    }

    private void OnMouseDown()
    {
        if (GameManager.instance.canClick)
        {
            spriteRenderer.sprite = pokemonImage;
            cardTurned = true;
            GameManager.instance.AddCardToCompare(gameObject);
        }
    }

    public void ReturnToOriginalPosition()
    {
        Debug.Log("Vratia nazad");
        //StopCoroutine(changeScaleCoroutine);
        changeScaleCoroutine = StartCoroutine(ChangeScale(initialTransform, 1f, true, true));
        //spriteRenderer.sprite = initialSprite;
    }

    public void RemoveFromField()
    {
        float delayBeforeDestroy = 1f;
        StartCoroutine(DelayAndDestroy(delayBeforeDestroy));
    }

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        initialSprite = spriteRenderer.sprite;
        //spriteRenderer.sprite = availableSprites1[Random.Range(0, availableSprites1.Length)];
        initialTransform = transform.localScale;
        targetTransform = transform.localScale + new Vector3(0.5f, 0.5f, 0f);
    }
}