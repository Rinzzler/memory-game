﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine;
using TMPro;

public class GetRequest : MonoBehaviour {

    public List<TextMeshProUGUI> scoreText;


    [System.Serializable]
    public class Score
    {
        public int id;
        public string name;
        public int clicks;
        public int pairs;
        public string time;
    }

    string fixJson(string value)
    {
        value = "{\"Items\":" + value + "}";
        return value;
    }

    public void StartGetText()
    {
        StartCoroutine(GetText());
    }


    public IEnumerator GetText()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://memorygameapi20171230021939.azurewebsites.net/api/score");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            string jsonString = fixJson(www.downloadHandler.text);
            string shortened = www.downloadHandler.text.Substring(1, www.downloadHandler.text.Length - 2);
            Debug.Log(shortened);
            Score[] myObject = JsonHelper.FromJson<Score>(jsonString);
            Debug.Log(myObject[0].name);
            var top10 = (from x in myObject orderby x.clicks ascending select x).Take(10).ToArray();
            for (int i = 0; i < 10; i++)
            {
                //Debug.Log(i + ". has this many clicks " + top10[i].clicks);
                scoreText[i].SetText(top10[i].name + " - " + top10[i].clicks + " clicks");                
            }

            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
        }
    }
}
