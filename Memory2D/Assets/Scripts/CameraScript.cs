﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.position = new Vector3(PlayerData.numberOfRows_Columns / 2 - 0.5f, PlayerData.numberOfRows_Columns / 2 - 0.5f, -10); //stavi varijable iz PlayerData
        gameObject.GetComponent<Camera>().orthographicSize = PlayerData.numberOfRows_Columns / 2 + 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
