﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormatTime : MonoBehaviour {

	public void Format(float time)
    {
        string hours = ((int)(time / 3600)).ToString("00");

        string minutes = ((int)((time % 3600) / 60)).ToString("00");

        string seconds = ((int)((time % 3600) % 60)).ToString("00");

        PlayerData.timeToFinishGame = hours + ":" + minutes + ":" + seconds;
        //Debug.Log("Time it took to finish game is " + PlayerData.timeToFinishGame);
    }
}
