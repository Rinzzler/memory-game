﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EndGameScript : MonoBehaviour {

    public InputField inputField;
    public GameObject panel_1;
    public GameObject panel_2;
    public TextMeshProUGUI scoreText;

    [System.Serializable]
    public class PostScore
    {
        public string name;
        public int clicks;
        public int pairs;
        public string time;
    }

    public void Submit()
    {
        PostScore postScore = new PostScore();
        postScore.clicks = 4;
        postScore.pairs = PlayerData.numberOfRows_Columns * PlayerData.numberOfRows_Columns / 2;
        InputField ajme = inputField;
        Debug.Log(ajme.text);
        postScore.name = ajme.text;
        postScore.time = PlayerData.timeToFinishGame;

        string url = "http://memorygameapi20171230021939.azurewebsites.net/api/score";

        string jsonFormat = JsonUtility.ToJson(postScore);


        var encoding = new System.Text.UTF8Encoding();
        var postHeader = new Hashtable();
        postHeader.Add("Content-Type", "application/json; charset=utf-8");

        WWW www = new WWW(url, encoding.GetBytes(jsonFormat), postHeader);

        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
            BackToMainMenu();
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        ResetPlayerData();
    }

    private void ResetPlayerData()
    {
        PlayerData.numberofClicks = 0;
        PlayerData.numberOfRows_Columns = 4;
        PlayerData.timeToFinishGame = "";
    }

    public void BackToGame()
    {
        panel_1.SetActive(false);
        GameManager.instance.canClick = true;
    }


    public void QuitGameQuestion()
    {
        if (GameManager.instance.canClick == true)
        {
            panel_1.SetActive(true);
            GameManager.instance.canClick = false;
        }
    }

    public void ShowScore()
    {
        scoreText.SetText("SCORE: " + (PlayerData.numberofClicks).ToString());
        Debug.Log(PlayerData.numberofClicks);
    }
}
